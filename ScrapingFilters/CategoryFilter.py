import requests


class CategoryFilter:
    def __init__(self, project, category):
        self.__db_url = "http://13.213.151.21:8000/projects"
        self.project = self.__get_project(project)
        self.category = self.__get_category(category)

    def __get_project(self, project):
        try:
            res = requests.get(f"{self.__db_url}/{project}")
            if not res.status_code == 200:
                raise ConnectionError
            else:
                return res.json()
        except ConnectionError:
            print("API Connection error")

    def __get_category(self, category):
        try:
            if self.project['project']:
                parameters = self.project['project']['parameters']
                for p in parameters:
                    if p['name'] == "category":
                        cat_urls = []
                        if category == "all":
                            for cat in p['choices']:
                                cat_urls.append(cat['value'])
                        else:
                            for cat in p['choices']:
                                if cat['value'] == category:
                                    cat_urls.append(cat['value'])
                        return cat_urls
        except KeyError:
            print("JSON Error")
        except IndexError:
            print("Index error")
        except:
            print("Unexpected Error")
        return []
