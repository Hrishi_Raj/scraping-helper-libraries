import json
import requests
from itemadapter import ItemAdapter


class Exporter:
    def __init__(self, project_name, task_id=None):
        self.__project_name = project_name
        self.__task_id = task_id
        self.__request_headers = {
            'Content-Type': 'application/json'
        }
        self.__base_url = "http://localhost:8080/tasks/"

    def set_task_id(self, task_id):
        self.__task_id = task_id

    def create_job(self):
        payload = json.dumps({
            "projectName": self.__project_name,
            "taskId": self.__task_id
        })
        url = self.__base_url + "create"
        if self.__task_id:
            requests.post(url=url, headers=self.__request_headers, data=payload)

    def finish_job(self):
        payload = json.dumps({
            "taskId": self.__task_id
        })
        url = self.__base_url + "finish"
        if self.__task_id:
            requests.post(url=url, headers=self.__request_headers, data=payload)

    def export_item(self, item, item_type):
        item = self.strip_item_fields(item)
        payload = json.dumps({
            "item": dict(item),
            "name": item_type,
            "taskId": self.__task_id
        })
        url = self.__base_url + "add-item"
        if self.__task_id:
            requests.post(url=url, headers=self.__request_headers, data=payload)

    def strip_item_fields(self, item):
        adapter = ItemAdapter(item)
        fields = adapter.field_names()
        for field in fields:
            adapter[field] = adapter[field].strip()
        return item
