from datetime import datetime


class DateFilter:
    def __init__(self, start_date, end_date, date_format):
        self.__format = date_format
        self.__start_date = self.__convert(start_date)
        self.__end_date = self.__convert(end_date)

    # check if date is in range
    def filter(self, date):
        date = self.__convert(date)
        return self.__start_date <= date <= self.__end_date

    # filter out list of articles provided corresponding date list with date
    def filter_list(self, date_list, news_list):
        filtered_list = []
        for i in range(len(date_list)):
            if self.filter(date_list[i]):
                filtered_list.append(news_list[i])
        return filtered_list

    def __convert(self, date):
        if isinstance(date, datetime):
            return date
        try:
            date = datetime.strptime(date, self.__format)
        except TypeError:
            print("Date Format Error")
        except:
            print("Date Conversion Failed")
        return date

    def change_format(self, date_format):
        self.__format = date_format
