from setuptools import setup

setup(
    name='ScrapingFilters',
    version='0.1.0',
    description='A scrapy project filters library',
    url='https://gitlab.com/Hrishi_Raj/scraping-helper-libraries.git',
    author='Hrishi Raj',
    packages=['ScrapingFilters'],
    install_requires=['requests',
                      'datetime',
                      'scrapy',
                      'itemadapter'
                      ],

    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD License',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
)