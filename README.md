# Scraping Helper Libraries

Libraries to be used with any scrapy project

## Installing Library
 
To install these as python library in your development environment use following command
```
pip install git+https://gitlab.com/Hrishi_Raj/scraping-helper-libraries.git@main
  or 
pip install git+https://gitlab.com/Hrishi_Raj/scraping-helper-libraries.git@main#egg=custom_name_here
```
Default name is ScrapingFilters.

## Get Started with import

```
from custom_name_here import Exporter
```

